import gettext
import os
import sys


def _translation_path():
    import noor.quran
    return noor.quran.get_path('locale', 'locale')

def set_lang(lang=None):
    langs = None
    if lang:
        langs = [lang]
    t = gettext.translation('noor', _translation_path(),
                            fallback=True, languages=langs)
    t.install(unicode=True)
    

set_lang()
