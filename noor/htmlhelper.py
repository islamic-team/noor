from noor import uihelpers


def get_sura_page(quran, trans, config, sura_number):
    result = []
    write_aya = _HTMLWriteAya(config)
    sura_text = uihelpers.write_sura(quran, config, sura_number,
                                     write_aya, trans)
    sura = quran.suras[sura_number - 1]
    header = '<head><title>Noor - %d. %s</title></head>\n' % \
             (sura_number, sura.name)
    result.append(header)
    result.append(_get_body_tag(config))
    result.append(sura_text)
    result.append('</body>')
    return _wrap_with_html_tag(''.join(result))


def get_suralist_page(quran, config, sura_href):
    """Generate a html page containing links to suras

    The `sura_href` parameter is a function that takes a number and
    returns the address of the sura.

    """
    result = []
    font = _get_font_tag(uihelpers.get_option(config, 'html.font'))
    result.append('<head><title>Noor - %s</title></head>\n' % _('Sura List'))
    result.append(_get_body_tag(config))
    result.append('<table border=1>\n')
    result.append(
        '<tr><td>%s</td><td>%s</td><td>%s</td></tr>\n'
        % (_('Index'), _('Sura Name'), _('Aya Count')))
    for index, sura in enumerate(quran.suras):
        sura_name = '<a href="%s">%s</a>' % (sura_href(index + 1),
                                             _wrap_text(sura.name, font))
        aya_count = uihelpers._int_to_str(len(sura.ayas), config=config)
        number = uihelpers._int_to_str(index + 1, config=config)
        result.append('<tr><td>%s</td><td>%s</td><td>%s</td></tr>\n' %
                      (number, sura_name, aya_count))
    result.append('</table>\n')
    result.append('<hr>')
    result.append(_('Generated by %s') %
                  ('<a href="http://noor.sf.net/">%s</a>' % _('Noor')))
    result.append('</body>')
    return _wrap_with_html_tag(''.join(result))


def _get_body_tag(config):
    bgcolor = uihelpers.get_option(config, 'html.bgcolor', None)
    fgcolor = uihelpers.get_option(config, 'html.fgcolor', None)
    others = ''
    if bgcolor:
        others += ' bgcolor="%s"' % bgcolor
    if fgcolor:
        others += ' text="%s"' % fgcolor
    return '<body%s>' % others


def _wrap_with_html_tag(text):
    html_attrs = ''
    result = []
    result.append('<?xml version="1.0" encoding="utf-8"?>\n')
    result.append(
        '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"\n')
    result.append('"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\n')
    result.append('<html xmlns="http://www.w3.org/1999/xhtml" '
                  'xml:lang="en" lang="en" dir="rtl">\n')
    result.append(text)
    result.append('</html>\n')
    return(''.join(result))


class _HTMLWriteAya(object):

    def __init__(self, config):
        self.font = _get_font_tag(uihelpers.get_option(config, 'html.font'))
        trans_path = uihelpers.get_path(config, 'common.translation')
        if trans_path is not None:
            trans_info = uihelpers._zekr_translation_info(trans_path)
        else:
            trans_info = {}
        self.trans_dir = trans_info.get('direction', None)
        raw_font = uihelpers.get_option(config, 'html.translation_font')
        self.trans_font = _get_font_tag(raw_font)
        self.number = 0

    def __call__(self, text, number=None, sajda=None,
                 translation=None, juz_start=None):
        result = []
        if juz_start is not None:
            result.append('<font color="green" size=+2><b>[%s]'
                          '</b></font><br>\n' % juz_start)
        result.append(self._get_anchor(number))
        result.append(_wrap_text(text, self.font))

        if number is not None:
            sign = number
            if sajda == 'minor':
                sign = '<font color="red">*%s*</font>' % sign
            if sajda == 'major':
                sign = '<font color="red">**%s**</font>' % sign
            result.append('<font color="blue">[%s]</font><br>\n' % sign)
        else:
            result.append('<br>\n')
        if translation is not None:
            if self.trans_dir:
                result.append('<div dir="%s">' % self.trans_dir)
            result.append(_wrap_text(translation, self.trans_font))
            if self.trans_dir:
                result.append('</div>')
        result.append('<hr>\n')
        return ''.join(result)

    def _get_anchor(self, number):
        if number is not None:
            self.number += 1
            return '<a name="%s"></a>\n' % self.number
        return ''


def _wrap_text(text, font):
    if font is not None:
        start, end = font
        return start + text + end
    return text


def _get_font_tag(raw_font):
    if raw_font is None:
        return None
    font = uihelpers.parse_font(raw_font)
    attrs = []
    if font.face is not None:
        attrs.append(('face', font.face))
    if font.size is not None:
        attrs.append(('size', font.size))
    if font.fgcolor is not None:
        attrs.append(('color', font.fgcolor))
    start = '<font %s >' % ' '.join(('%s="%s"' % (attr, value))
                                    for attr, value in attrs)
    end = '</font>'
    return start, end
