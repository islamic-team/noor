import re

import gtk.keysyms

import noor.gtkhelper.keys
import noor.uihelpers


class Searcher(object):

    def __init__(self, main):
        self.main = main
        self._init_bar()
        self._init_text()
        self._init_tag()
        self.state = ForwardSearch()

    def _init_bar(self):
        self.box = gtk.HBox()
        self.entry = gtk.Entry()
        self.entry.connect('key-release-event', self._key_released)
        self.handle_key = noor.gtkhelper.keys.Keys(
            self.keys, [], self.c_keys, self.m_keys, [], [self], False)
        self.entry.connect('key-press-event', self._key_pressed)
        self.box.pack_start(self.entry, True, True, 0)

        if not self._option('gtk.hide_buttons', False):
            next = gtk.Button()
            next_label = gtk.Label()
            next_label.set_text(_('Next Match'))
            next.add(next_label)
            prev_label = gtk.Label()
            prev_label.set_text(_('Previous Match'))
            prev = gtk.Button()
            prev.add(prev_label)

            close_label = gtk.Label()
            close_label.set_text(_('X'))
            close = gtk.Button()
            close.add(close_label)

            self.box.pack_start(prev, True, True, 0)
            self.box.pack_start(next, True, True, 0)
            self.box.pack_start(close, False, False, 0)
            prev.connect('clicked', self.backward)
            next.connect('clicked', self.forward)
            close.connect('clicked', self.close)

        self.main.add_hook('sura', self.close)

    def _init_text(self):
        text = self.buffer.get_text(*self.buffer.get_bounds())
        self.original_text = text.decode('utf-8')
        self.text = self._normtext(self.original_text)
        mark = self.buffer.get_mark('location')
        self.start = self._get_offset(self.buffer.get_iter_at_mark(mark))

    def _normtext(self, text):
        if not isinstance(text, unicode):
            text = text.decode('utf-8')
        if self._option('gtk.ignore_case'):
            text = text.lower()
        if self._option('gtk.ignore_diacritics'):
            text = diacritic_pattern.sub('', text)
        return text

    def _option(self, option, default=True):
        return noor.uihelpers.get_option(self.main.config, option, default)

    def search(self):
        self.main.minibuffer.show(self.box)
        self.entry.grab_focus()

    def close(self, *args):
        self.main.remove_hook('sura', self.close)
        self.main.minibuffer.hide()

    def _key_pressed(self, widget, event, data=None):
        return self.handle_key(widget, event, data)

    def _key_released(self, widget, event, data=None):
        self._update_result()

    def _update_result(self):
        match = self._find_match()
        if match is not None:
            start_iter = self._get_iter(match[0])
            end_iter = self._get_iter(match[1])
            self.buffer.apply_tag_by_name('found', start_iter, end_iter)
            self.view.scroll_to_iter(start_iter, 0, True, 0.5, 0.5)

    _offset_cache = (0, -1)
    def _get_iter(self, offset):
        if self._option('gtk.ignore_diacritics'):
            i, current = self._offset_cache
            if current > offset:
                i, current = (0, -1)
            while i < len(self.original_text) and current < offset:
                if self.original_text[i] not in diacritics:
                    current += 1
                i += 1
            # HACK: ... cached .. [safe range] .. cached + 37 ...
            if i < self._offset_cache[0] or self._offset_cache[0] + 37 < i:
                self._offset_cache = (i, current)
        return self.buffer.get_iter_at_offset(i - 1)

    def _get_offset(self, iter):
        offset = iter.get_offset()
        if self._option('gtk.ignore_diacritics'):
            for c in self.original_text[:offset]:
                if c in diacritics:
                    offset -= 1
        return offset

    def _find_match(self):
        word = self.entry.get_text()
        bounds = self.buffer.get_bounds()
        self.buffer.remove_tag_by_name('found', bounds[0], bounds[1])
        word = self._normtext(word)

        try:
            if word:
                start = self.state.search(self.text, word, self.start)
                return start, start + len(word)
        except ValueError:
            pass

    def forward(self, *args):
        self._next(ForwardSearch)

    def backward(self, *args):
        self._next(BackwardSearch)

    def _next(self, class_):
        if isinstance(self.state, class_):
            match = self._find_match()
            self.start = self.state.next(self.text, match)
        else:
            self.state = class_()
        self._update_result()

    def _init_tag(self):
        try:
            self.buffer.create_tag('found', background='#aaddaa')
        except TypeError:
            pass

    @property
    def buffer(self):
        return self.view.get_buffer()

    @property
    def view(self):
        return self.main.text_view

    keys = {
        gtk.keysyms.Down: forward,
        gtk.keysyms.Escape: close,
        gtk.keysyms.Return: close,
        gtk.keysyms.Up: backward,}
    c_keys = {
        ord('g'): close,
        ord('r'): backward,
        ord('s'): forward,}
    m_keys = {
        ord('n'): forward,
        ord('p'): backward,}

class ForwardSearch(object):

    def next(self, text, match):
        if match is not None:
            return match[1]
        return 0

    def search(self, text, word, start):
        return text.index(word, start)

class BackwardSearch(object):

    def next(self, text, match):
        if match is not None:
            return match[0]
        return len(text)

    def search(self, text, word, start):
        return text.rindex(word, 0, start)

diacritics = u'\u0652\u0651\u0650\u064f\u064e\u064d\u064c\u064b\u0670'
diacritic_pattern = re.compile('[%s]' % diacritics)
