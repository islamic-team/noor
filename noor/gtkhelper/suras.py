import gtk

from noor import uihelpers


def ask_sura(config, quran):
    dialog = SurasDialog(config, quran)
    dialog.run()
    return dialog.get_sura()


class SurasDialog(object):

    def __init__(self, config, quran):
        self.dialog = gtk.Dialog()
        self.dialog.set_title('Noor - ' + _('Choose Sura'))
        self.dialog.set_border_width(1)
        self.dialog.set_size_request(400, 200)
        self.dialog.connect('destroy', self._quit)

        l = gtk.Label()
        l.set_markup(_('Choose a sura to display:'))
        self.combobox = gtk.combo_box_entry_new_text()
        for index, sura in enumerate(quran.suras):
            n = uihelpers._int_to_str(index + 1, config=config)
            self.combobox.append_text('%s - %s' % (n, sura.name))
        self.combobox.set_active(0)

        button = gtk.Button()
        button.connect('clicked', self._selected)
        button_label = gtk.Label()
        button_label.set_markup(_('Show'))
        button.add(button_label)

        self.dialog.vbox.pack_start(l, True, True, 0)
        self.dialog.vbox.pack_start(self.combobox, True, True, 0)
        self.dialog.vbox.pack_start(button, True, True, 0)
        self.selected = False

    def _quit(self, *args):
        gtk.main_quit()

    def _selected(self, *args):
        self.selected = True
        self.dialog.hide()
        self._quit()

    def run(self):
        self.dialog.show_all()
        gtk.main()

    def get_sura(self):
        if self.selected:
            return self.combobox.get_active() + 1
