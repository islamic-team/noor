import gtk

import noor.gtkhelper
import noor.uihelpers


def make_menu_bar(main):
    if not noor.uihelpers.get_option(main.config, 'gtk.menu_bar', True):
        return None
    menus = [
        (_('File'),
         (_('Exit'), main._quit, 'exit13.png'),
         ),
        (_('View'),
         (_('Reload'), main._current_aya, 'reload13.png'),
         (_('Random Aya'), main._goto_random_aya, 'random13.png'),
         None,
         (_('Choose Sura'), main._ask_sura, 'go13.png'),
         None,
         (_('Back'), main._back, 'history13.png'),
         (_('Forward'), main._forward, 'history13.png'),
         None,
         (_('Fullscreen'), main._fullscreen, 'fullscreen13.png'),
         None,
         (_('Go to'),
          (_('Next Sura'), main._next_sura, 'next213.png'),
          (_('Previous Sura'), main._prev_sura, 'prev213.png'),
          (_('Next Aya'), main._next_aya, 'next113.png'),
          (_('Previous Aya'), main._prev_aya, 'prev113.png'),
          None,
          (_('Go to Aya'), main._goto_aya, 'goto13.png'),
          (_('Go to Sura'), main._goto_sura, 'goto13.png'),
          (_('Go to Juz'), main._goto_juz, 'goto13.png'),
          ),
         ),
        (_('Search'),
         (_('Search Forward'), main._search_forward, 'search13.png'),
         (_('Search Backward'), main._search_backward, 'search13.png'),
         ),
        (_('Bookmarks'),
         (_('Add Bookmark'), main._add_bookmark, 'bookmark_add13.png'),
         (_('Show Bookmarks'), main._list_bookmarks, 'bookmark13.png'),
         ),
        (_('Help'),
         (_('About'), main._show_about, 'about13.png'),
         ),
        ]

    menu_bar = gtk.MenuBar()
    _add_menu(menus, menu_bar)
    return menu_bar

def _add_menu(menus, parent=None):
    if parent:
        menu = parent
    else:
        menu = gtk.Menu()
    for submenu in menus:
        if submenu is None:
            menu.add(gtk.SeparatorMenuItem())
            continue
        item = gtk.ImageMenuItem(submenu[0])
        if isinstance(submenu[1], tuple):
            newmenu = _add_menu(submenu[1:])
            item.set_submenu(newmenu)
        elif submenu[1] is not None:
            item.connect('activate', submenu[1])
            if len(submenu) > 2:
                item.set_image(noor.gtkhelper._get_image(submenu[2]))
        menu.append(item)
    return menu
