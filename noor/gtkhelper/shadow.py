import noor.uihelpers


def init_shadow(main):
    _Shadow(main)

class _Shadow(object):

    def __init__(self, main):
        self.main = main
        if noor.uihelpers.get_option(main.config, 'gtk.shadow_ayas', False):
            self.main.add_hook('sura', self._sura)
            self.main.add_hook('aya', self._aya)

    def _sura(self):
        self.buffer.create_tag('shadow', background='#BBBBBB',
                               foreground='#888888')

    def _aya(self):
        start, end = self.buffer.get_bounds()
        self.buffer.remove_tag_by_name('shadow', start, end)
        aya = self.main.current_aya
        if aya:
            mark = self.buffer.get_mark(str(aya))
            next_mark = self.buffer.get_mark(str(aya + 1))
            self.buffer.apply_tag_by_name(
                'shadow', start, self.buffer.get_iter_at_mark(mark))
            if next_mark:
                self.buffer.apply_tag_by_name(
                    'shadow', self.buffer.get_iter_at_mark(next_mark), end)

    @property
    def buffer(self):
        return self.main.text_view.get_buffer()
