class Marks(object):

    def __init__(self, main):
        self.marks = {}
        self.main = main

    def add_mark(self, name):
        self.marks[name] = (self.main.current_sura,
                            self.main.current_aya)

    def jump_mark(self, name):
        if name in self.marks:
            sura, aya = self.marks[name]
            if sura and sura != self.main.current_sura:
                self.main.show_sura(sura)
            if aya and aya != self.main.current_aya:
                self.main.show_aya(aya)
