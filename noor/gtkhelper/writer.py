import gtk

from noor import uihelpers


class GtkAyaWriter(object):

    def __init__(self, text_view, config):
        self.view = text_view
        self.buffer = text_view.get_buffer()
        self.result = []
        self._create_tags(config)
        self.view.set_editable(False)
        self.view.set_cursor_visible(False)
        self.view.set_wrap_mode(gtk.WRAP_WORD)
        self.number = 0
        self.juz_number = 0
        self.sajda_number = 0

    def _create_tags(self, config):
        fgcolor = uihelpers.get_option(config, 'gtk.fgcolor')
        bgcolor = uihelpers.get_option(config, 'gtk.bgcolor')
        dirs = {'rtl': gtk.TEXT_DIR_RTL, 'ltr': gtk.TEXT_DIR_LTR}
        font = uihelpers.get_option(config, 'gtk.font', '')
        trans_font = uihelpers.get_option(config, 'gtk.translation_font', '')
        attrs = {'direction': dirs['rtl']}
        if fgcolor:
            attrs['foreground'] = fgcolor
        if bgcolor:
            attrs['background'] = bgcolor
        attrs1 = dict(attrs)
        attrs1.update(self._font_attrs(font))
        attrs2 = dict(attrs)
        attrs3 = dict(attrs1)
        attrs4 = dict(attrs1)
        attrs5 = dict(attrs1)
        attrs2.update(self._font_attrs(trans_font))
        attrs2['direction'] = direction=dirs[self._get_trans_dir(config)]
        attrs3['background'] = 'green'
        attrs4['foreground'] = 'blue'
        attrs5['foreground'] = 'red'
        self.tag1 = self.buffer.create_tag('quran', **attrs1)
        self.tag2 = self.buffer.create_tag('trans', **attrs2)
        self.tag3 = self.buffer.create_tag('juz', **attrs3)
        self.tag4 = self.buffer.create_tag('sign', **attrs4)
        self.tag5 = self.buffer.create_tag('sajda', **attrs5)

    def _font_attrs(self, rawfont):
        font = uihelpers.parse_font(rawfont)
        attrs = {}
        if font.face is not None:
            attrs['font'] = font.face
        if font.size is not None:
            attrs['size-points'] = int(font.size)
        if font.fgcolor is not None:
            attrs['foreground'] = font.fgcolor
        if font.bgcolor is not None:
            attrs['background'] = font.bgcolor
        return attrs

    def _get_trans_dir(self, config):
        trans_path = uihelpers.get_path(config, 'common.translation')
        if trans_path is not None:
            trans_info = uihelpers._zekr_translation_info(trans_path)
        else:
            trans_info = {}
        return trans_info.get('direction', 'rtl')

    def __call__(self, text, number=None, sajda=None,
                 translation=None, juz_start=None):
        if juz_start is not None:
            self._insert(juz_start.ljust(2), self.tag3)
            self._insert('\n')
        self._set_aya_mark(number)
        self._insert(text, self.tag1)
        # handling aya sign
        if number is not None:
            sign = str(number)
            sign_tag = self.tag4
            if sajda == 'minor':
                sign = '*%s*' % sign
            elif sajda == 'major':
                sign = '**%s**' % sign
            if sajda is not None:
                sign_tag = self.tag5
            self._insert('[', self.tag4)
            self._insert('%s' % sign, sign_tag)
            self._insert(']\n', self.tag4)
        else:
            self._insert('\n')
        if translation is not None:
            self._insert(translation + '\n', self.tag2)
        self._insert('\n')

    def _set_aya_mark(self, number):
        if number:
            self.number += 1
            self._set_mark(str(self.number))

    def _set_mark(self, name):
        textiter = self.buffer.get_iter_at_mark(self.buffer.get_insert())
        mark = self.buffer.create_mark(name, textiter, left_gravity=True)

    def _insert(self, text, *tags):
        self.buffer.insert_with_tags(self.buffer.get_end_iter(),
                                     text, *tags)

    @property
    def text(self):
        return ''.join(self.result)
