import os

import gtk

import noor.quran


def _get_image(name):
    icons = noor.quran.get_path('data/icons', 'pixmaps/noor')
    icon = gtk.Image()
    icon.set_from_file(os.path.join(icons, name))
    return icon
