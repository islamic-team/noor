import gtk

import os

import noor.gtkhelper.ask
import noor.uihelpers


def add_bookmark(main):
    if not main.current_sura:
        return
    path = _bookmark_path(main)
    if path:
        name = noor.gtkhelper.ask.ask(_('Add Bookmark'), _('Bookmark Name'))
        if name:
            line = '%s:%s %s\n' % (main.current_sura, main.current_aya, name)
            output = open(path, 'ab')
            output.write(line.encode('utf-8'))
            output.close()


def list_bookmarks(main):
    path = _bookmark_path(main)
    if path and os.path.exists(path):
        file = open(path, 'rb')
        data = file.read()
        file.close()
        text = data.decode('utf-8')
        bookmarks = []
        for line in text.splitlines():
            if not line.strip() or line.lstrip().startswith('#'):
                continue
            location, name = line.split(' ', 1)
            sura, aya = location.split(':')
            bookmarks.append((int(sura), int(aya), name))
        bookmark = _ask_bookmark(bookmarks, main.config, main.quran)
        if bookmark:
            sura, aya, name = bookmark
            main.show_sura(sura)
            main.show_aya(aya)


def _bookmark_path(main):
    path = noor.uihelpers.get_option(
        main.config, 'gtk.bookmark_file', '~/.noor.bmk')
    if path:
        return os.path.expandvars(os.path.expanduser(path))


def _ask_bookmark(bookmarks, config, quran):
    dialog = gtk.Dialog()
    dialog.set_title('%s - %s' % (_('Noor'),  _('Jump to Bookmark')))
    dialog.set_border_width(1)
    dialog.set_size_request(400, 200)

    def _quit(*args):
        dialog.hide_all()
        gtk.main_quit()
    dialog.connect('destroy', _quit)

    l = gtk.Label()
    l.set_markup(_('Select a bookmark below'))
    combobox = gtk.combo_box_entry_new_text()
    for sura, aya, name in bookmarks:
        sura_name = quran.suras[sura - 1].name
        sura = noor.uihelpers._int_to_str(sura, config=config)
        aya = noor.uihelpers._int_to_str(aya, config=config)
        combobox.append_text('%s   --   %s %s:%s' %
                             (name, sura_name, sura, aya))
    combobox.set_active(0)

    class _Selected(object):
        selected = False
        def __call__(self, *args):
            self.selected = True
            _quit()
    _selected = _Selected()
    button = gtk.Button()
    button.connect('clicked', _selected)
    button_label = gtk.Label()
    button_label.set_markup(_('Jump'))
    button.add(button_label)

    dialog.vbox.pack_start(l, True, True, 0)
    dialog.vbox.pack_start(combobox, True, True, 0)
    dialog.vbox.pack_start(button, True, True, 0)

    dialog.show_all()
    gtk.main()
    if _selected.selected:
        return bookmarks[combobox.get_active()]
