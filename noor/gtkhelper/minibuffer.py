import gtk


class MiniBuffer(object):

    def __init__(self, main):
        self.main = main
        self.minibuffer = gtk.VBox()

    def show(self, widget):
        self.hide()
        self.minibuffer.add(widget)
        self.minibuffer.show_all()
        widget.grab_focus()

    def hide(self):
        self.minibuffer.hide_all()
        for child in self.minibuffer.get_children():
            self.minibuffer.remove(child)
        self.main.text_view.grab_focus()
