import os
import subprocess
import threading
import string

import gobject

from noor import uihelpers


class Player(object):

    def __init__(self, main):
        self.main = main
        self.main.add_hook('sura', self._sura)
        self.main.add_hook('aya', self._aya)
        self.follow = uihelpers.get_option(main.config,
                                           'gtk.follow_ayas', False)
        gobject.threads_init()
        self.handles = []

    def _aya(self):
        self._run_hook('gtk.play_aya')

    def _sura(self):
        self._run_hook('gtk.play_sura')

    def stop(self):
        self._kill_players()

    def _run_hook(self, name):
        hook = uihelpers.get_option(self.main.config, name)
        mapping = {'sura': str(self.main.current_sura).rjust(3, '0'),
                   'aya': str(self.main.current_aya).rjust(3, '0')}
        if hook:
            hook = string.Template(hook).substitute(mapping)
            self._kill_players()
            process = subprocess.Popen(hook, shell=True)
            if self.follow:
                handle = _HandleProcess(self, process)
                threading.Thread(target=handle).start()

    def _kill_players(self):
        for handle in self.handles:
            handle.kill()


class _HandleProcess(object):

    def __init__(self, player, process):
        self.player = player
        self.main = player.main
        self.process = process
        self.quit = False
        player.handles.append(self)

    def __call__(self):
        try:
            self.main.add_hook('kill', self.kill)
            while not self.quit and self.process.poll() is None:
                self.process.wait()
            if self.process.poll() == 0 and not self.quit:
                def next():
                    if not self.quit:
                        self.main._next_aya()
                gobject.idle_add(next)
            elif self.quit:
                if hasattr(self.process, 'terminate'):
                    self.process.terminate()
        finally:
            self.main.remove_hook('kill', self.kill)
            self.player.handles.remove(self)

    def kill(self):
        self.quit = True
        if self.process.poll() is not None:
            return
        try:
            if hasattr(self.process, 'terminate'):
                self.process.terminate()
            elif os.name != 'nt':
                os.kill(self.process.pid, 9)
            else:
                import ctypes
                ctypes.windll.kernel32.TerminateProcess(
                    int(self.process._handle), -1)
        except (OSError, ImportError):
            pass
