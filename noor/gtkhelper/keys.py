import gtk.gdk


class Keys(object):

    def __init__(self, keys=[], s_keys=[], c_keys=[],
                 m_keys=[], cm_keys=[], args=[], grab_inserts=True):
        self.args = args
        self.keys = keys
        self.s_keys = s_keys
        self.c_keys = c_keys
        self.m_keys = m_keys
        self.cm_keys = cm_keys
        self.grab_inserts = grab_inserts

    def __call__(self, widget, event, data=None):
        if event.type == gtk.gdk.KEY_PRESS:
            keyval = event.keyval
            keys = None
            grab = False
            if (event.state & gtk.gdk.CONTROL_MASK and
                event.state & gtk.gdk.MOD1_MASK):
                keys = self.cm_keys
            elif event.state & gtk.gdk.CONTROL_MASK:
                keys = self.c_keys
            elif event.state & gtk.gdk.MOD1_MASK:
                keys = self.m_keys
            elif event.state & gtk.gdk.SHIFT_MASK:
                keys = self.s_keys
                grab = self.grab_inserts
            else:
                keys = self.keys
                grab = self.grab_inserts
            if keyval in keys:
                keys[keyval](*self.args)
                grab = True
            return grab
