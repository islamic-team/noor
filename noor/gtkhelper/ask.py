import gtk


class _ReadKey(object):

    key = None

    def __call__(self, widget, event, data=None):
        if event.type == gtk.gdk.KEY_PRESS:
            keyval = event.keyval
            keys = None
            grab = False
            if event.state & (gtk.gdk.CONTROL_MASK |
                              gtk.gdk.MOD1_MASK):
                pass
            if chr(keyval).isalnum():
                self.key = chr(keyval)
            gtk.main_quit()
            return True

def ask_key():
    dialog = gtk.Dialog()
    dialog.set_title('%s - %s' % (_('Noor'), 'mark'))
    dialog.set_border_width(1)
    dialog.set_size_request(210, 145)
    def quit(*args, **kwds):
        gtk.main_quit()
    dialog.connect('destroy', lambda *args: quit())
    dialog.connect('delete_event', lambda *args: quit())
    box = dialog.vbox
    l = gtk.Label()
    l.set_markup('mark name:')
    box.add(l)
    dialog.show_all()
    keys = _ReadKey()
    dialog.connect('key_press_event', keys)
    gtk.main()
    dialog.hide()
    return keys.key

def ask(title, message):
    dialog = gtk.Dialog()
    dialog.set_title('%s - %s' % (_('Noor'), title))
    dialog.set_border_width(1)
    dialog.set_size_request(210, 145)
    def quit(*args, **kwds):
        dialog.hide()
        if not kwds.get('done', False):
            entry.set_text('')
        gtk.main_quit()
    dialog.connect('destroy', lambda *args: quit())
    dialog.connect('delete_event', lambda *args: quit())
    box = dialog.vbox
    l = gtk.Label()
    l.set_markup(message)
    box.add(l)
    entry = gtk.Entry()
    def selected(*args):
        quit(done=True)
    entry.connect('activate', selected)
    box.add(entry)
    dialog.show_all()
    gtk.main()
    return entry.get_text()
