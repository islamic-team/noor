import gtk

from noor import uihelpers


class Status(object):

    def __init__(self, main):
        self.main = main
        if uihelpers.get_option(main.config, 'gtk.status_bar', True):
            self._init_status_bar()
        else:
            self.status_bar = None

    def _init_status_bar(self):
        self.status_bar = gtk.Statusbar()
        self.location_id = self.status_bar.get_context_id('sura location')
        self.prefix_id = self.status_bar.get_context_id('prefix')
        self.status_bar.push(self.location_id, '')
        self.status_bar.push(self.prefix_id, '')
        self.update()
        self.main.add_hook('sura', self.update)
        self.main.add_hook('aya', self.update)

    def update(self):
        if self.status_bar is None:
            return
        main = self.main
        if main.current_sura:
            result = []
            sura_number = self._int_to_str(main.current_sura)
            result.append('%s: %s-%s' % (_('Sura'), sura_number,
                                         main.sura.name))
            juz_text = ', '.join(self._int_to_str(juz)
                                 for juz in main.sura.juz)
            result.append(_('Juz') + ': ' + juz_text)
            if main.current_aya:
                aya = self._int_to_str(main.current_aya)
                ayas = self._int_to_str(main.sura_ayas)
                result.append('%s: %s (%s)' % (_('Aya'), aya, ayas))
            text = (' ' * 19).join(result)
        else:
            text = _('About')
        self.status_bar.pop(self.location_id)
        self.status_bar.push(self.location_id, text)

    def prefix(self):
        if self.status_bar is None:
            return
        self.status_bar.pop(self.prefix_id)
        if self.main.number:
            number_text = self._int_to_str(self.main.number)
            text = '%s: %s' % (_('Prefix'), number_text)
            self.status_bar.push(self.prefix_id, text)

    def _int_to_str(self, number):
        return uihelpers._int_to_str(number, config=self.main.config)
