import gtk

import noor.gtkhelper
from noor import uihelpers


def make_toolbar(main):
    if not uihelpers.get_option(main.config, 'gtk.toolbar', True):
        return None
    toolbar_buttons = [
        (_('Previous Sura'), main._prev_sura, 'prev2.png'),
        (_('Next Sura'), main._next_sura, 'next2.png'),
        None,
        (_('Previous Aya'), main._prev_aya, 'prev1.png'),
        (_('Next Aya'), main._next_aya, 'next1.png'),
        None,
        (_('Choose Sura'), main._ask_sura, 'go2.png'),
        None,
        (_('Go to Aya'), main._goto_aya, 'goto16.png'),
        (_('Go to Sura'), main._goto_sura, 'goto16.png'),
        (_('Go to Juz'), main._goto_juz, 'goto16.png'),
        None,
        (_('Back'), main._back, 'history.png'),
        (_('Forward'), main._forward, 'history.png'),
        None,
        (_('Search'), main._search_forward, 'search16.png'),
        None,
        (_('Fullscreen'), main._fullscreen, 'fullscreen16.png'),
        ]

    styles = {'text': gtk.TOOLBAR_TEXT,
              'icons': gtk.TOOLBAR_ICONS,
              'both': gtk.TOOLBAR_BOTH}
    toolbar = gtk.Toolbar()
    toolbar.set_orientation(gtk.ORIENTATION_HORIZONTAL)
    style = uihelpers.get_option(main.config, 'gtk.toolbar_style', 'both')
    toolbar.set_style(styles.get(style, styles['both']))
    for index, item in enumerate(toolbar_buttons):
        if not item:
            toolbar.insert(gtk.SeparatorToolItem(), index)
            continue
        icon = None
        if len(item) > 2:
            icon = noor.gtkhelper._get_image(item[2])
        button = gtk.ToolButton(icon, item[0])
        button.connect('clicked', item[1])
        toolbar.insert(button, index)
    return toolbar
