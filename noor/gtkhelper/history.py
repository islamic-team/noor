class History(object):

    def __init__(self, main):
        self.main = main
        self.history = []
        self.undone_history = []
        self._history_lock = False
        self.main.add_hook('sura_planned', self._push)

    def _push(self, clear_undone=True):
        sura = self.main.current_sura
        if not self._history_lock and sura is not None:
            self.history.append((sura, self.main.current_aya))
            if clear_undone:
                del self.undone_history[:]

    def back(self, *args, **kwds):
        if self.history:
            sura, aya = self.history.pop()
            if self.main.current_sura is not None:
                self.undone_history.append((self.main.current_sura,
                                            self.main.current_aya))
            self._goto_history(sura, aya)

    def forward(self, *args, **kwds):
        if self.undone_history:
            sura, aya = self.undone_history.pop()
            self._push(clear_undone=False)
            self._goto_history(sura, aya)

    def _goto_history(self, sura, aya):
        try:
            self._history_lock = True
            self.main.show_sura(sura)
            self.main.show_aya(aya)
        finally:
            self._history_lock = False

