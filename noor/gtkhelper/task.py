import gtk

from noor import uihelpers


def create_task_pane(main):
    if not uihelpers.get_option(main.config, 'gtk.task_pane', False):
        return
    pane = gtk.VBox()
    sura_label = gtk.Label()
    sura_label.set_text(_('Sura'))
    sura = gtk.Entry()
    sura.set_max_length(3)
    def sura_selected(*args):
        main.show_sura(int(sura.get_text()))
    sura.connect('activate', sura_selected)
    pane.pack_start(sura_label, False, False, 0)
    pane.pack_start(sura, False, False, 0)

    aya_label = gtk.Label()
    aya_label.set_text(_('Aya'))
    aya = gtk.Entry()
    aya.set_max_length(3)
    def aya_selected(*args):
        main.show_aya(int(aya.get_text()))
    aya.connect('activate', aya_selected)
    pane.pack_start(aya_label, False, False, 0)
    pane.pack_start(aya, False, False, 0)

    goto = gtk.Button()
    def goto_location(*args):
        sura_selected()
        aya_selected()
    goto.connect('clicked', goto_location)
    goto_label = gtk.Label()
    goto_label.set_text(_('Show'))
    goto.add(goto_label)
    pane.pack_start(goto, False, False, 0)
    return pane
