import os.path

import gtk

import noor.gtkhelper.keys
from noor import uihelpers


class Notes(object):

    def __init__(self, main):
        self.main = main
        if not uihelpers.get_option(main.config, 'gtk.show_notes', False):
            self.box = None
            return
        self.main.add_hook('sura', self.update)
        self.main.add_hook('aya', self.update)
        self.dir = uihelpers.get_option(main.config, 'gtk.notes_dir',
                                        '~/.noornotes')
        self.dir = os.path.expanduser(self.dir)
        self.box = gtk.HBox()
        self.view = gtk.TextView()
        self.box.pack_start(self.view, True, True, 0)

        if not uihelpers.get_option(main.config, 'gtk.hide_buttons', False):
            close_label = gtk.Label()
            close_label.set_text(_('X'))
            close = gtk.Button()
            close.add(close_label)
            close.connect('clicked', self.toggle)
            self.box.pack_start(close, False, False, 0)

        self.location = (None, None)
        self.handle_key = noor.gtkhelper.keys.Keys(
            self.keys, [], self.c_keys, self.m_keys, [], [self], False)
        self.view.connect('key-press-event', self._key_pressed)
        self.shown = True

    def _key_pressed(self, widget, event, data=None):
        return self.handle_key(widget, event, data)

    def update(self):
        self.save()
        sura = self.main.current_sura
        aya = self.main.current_aya
        newdata = self._read(sura, aya)
        if newdata:
            self.buffer.set_text(newdata)
        else:
            self.buffer.set_text('')
        self.location = (sura, aya)

    def save(self):
        data = self.buffer.get_text(*self.buffer.get_bounds())
        if self.location != (None, None):
            self._write(self.location[0], self.location[1], data)

    @property
    def buffer(self):
        return self.view.get_buffer()

    def _read(self, sura, aya):
        filename = self._path(sura, aya)
        if filename and os.path.exists(filename):
            notefile = open(filename, 'rb')
            try:
                result = notefile.read().decode('utf-8')
                if result.endswith('\n'):
                    result = result[:-1]
                return result
            finally:
                notefile.close()

    def _write(self, sura, aya, data):
        filename = self._path(sura, aya)
        if filename:
            if not os.path.exists(self.dir):
                os.mkdir(self.dir)
            if data.strip():
                notefile = open(filename, 'wb')
                notefile.write(data.encode('utf-8'))
                notefile.write('\n')
                notefile.close()
            elif os.path.exists(filename):
                os.remove(filename)

    def _path(self, sura, aya):
        if sura is not None:
            if aya:
                filename = '%s-%s.txt' % (sura, aya)
            else:
                filename = '%s.txt' % sura
            return os.path.join(self.dir, filename)

    def _done(self):
        self.save()
        self.main.text_view.grab_focus()

    def write_note(self):
        if self.box:
            if not self.shown:
                self.toggle()
            self.view.grab_focus()

    def toggle(self, *args):
        if self.box:
            if self.shown:
                self.box.hide()
            else:
                self.box.show()
            self.shown = not self.shown
            self._done()

    keys = {gtk.keysyms.Escape: _done}
    c_keys = {ord('g'): _done, ord('s'): save}
    m_keys = {}
