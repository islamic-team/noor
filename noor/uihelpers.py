import ConfigParser
import os.path
import unicodedata
import zipfile

import noor.i18n
import noor.quran
from noor.quran import Quran, quran_from_path


def write_sura(quran, config, number, write_aya, translation=None):
    """Write a sura using `write_aya` callback

    The `write_aya` callback is a function with signature::

        write_aya(text, number=None, sajda=None, 
                  translation=None, juz_start=None)

    For each aya in the sura, `write_aya` is called.
    """
    sura = quran.suras[number - 1]
    if translation is not None:
        sura_trans = translation.suras[number - 1]
    result = []
    if number not in (1, 9):
        trans = None
        if translation is not None:
            trans = translation.get_in_the_name()
        current = write_aya(quran.get_in_the_name(), translation=trans)
        if current is not None:
            result.append(current)
    for aya in range(len(sura.ayas)):
        aya_index = (number, aya + 1)
        juz_start = None
        if aya_index in quran.juz_starts:
            juz_number = quran.juz_starts.index(aya_index) + 1
            juz_start = _int_to_str(juz_number, config=config)
        sajda = quran.sajdas.get(aya_index, None)
        trans = None
        if translation is not None:
            trans = sura_trans.ayas[aya]
        sura_number = _int_to_str(aya + 1, config=config)
        current = write_aya(sura.ayas[aya], number=sura_number, sajda=sajda,
                            translation=trans, juz_start=juz_start)
        if current is not None:
            result.append(current)
    return ''.join(result)

_zero = unicodedata.lookup('ARABIC-INDIC DIGIT ZERO')

def _int_to_str(number, arabic=False, config=None):
    if config is not None:
        arabic = get_option(config, 'common.arabic_numbers', False)
    if not arabic:
        return str(number)
    result = []
    for c in str(number):
        if c == '.':
            result.append('/')
        else:
            digit = ord(c) - ord('0')
            result.append(unichr(ord(_zero) + digit))
    return ''.join(result)


def write_aya(text, number=None, sajda=None,
              translation=None, juz_start=None):
    pass


def get_quran(config):
    path = get_path(config, 'common.quran')
    hidden = get_option(config, 'common.hide_chars', default='')
    return noor.quran.quran_from_path(path, replacements=_replacements(hidden))


def get_translation(config):
    trans = get_path(config, 'common.translation')
    hidden = get_option(config, 'common.hide_translation_chars', default='')
    if trans is not None and os.path.exists(trans):
        return _translation_from_path(trans, _replacements(hidden))

def _replacements(hidden):
    replacements = []
    for code in hidden.split('\t'):
        to = ''
        if '->' in code:
            code, to = code.split('->', 1)
        replacements.append((unicode(code, 'utf-8'), unicode(to, 'utf-8')))
    return replacements


def get_path(config, option):
    path = get_option(config, option, None)
    if path is not None:
        path = os.path.expandvars(os.path.expanduser(path))
        return path


def get_config(create=True):
    address = os.path.expanduser('~/.noor')
    if create and not os.path.exists(address):
        dot_noor = open(address, 'w')
        dot_noor.write(_DEFAULT_DOT_NOOR)
        dot_noor.close()
    config = ConfigParser.SafeConfigParser()
    config.read(address)
    return config


def set_lang(config):
    lang = get_option(config, 'common.lang')
    if lang is not None:
        noor.i18n.set_lang(lang)


class Font(object):

    face = None
    size = None
    fgcolor = None
    bgcolor = None

def parse_font(name):
    tokens = [token.strip() for token in name.split(' ')]
    result = Font()
    for token in tokens:
        if token.isdigit():
            result.size = token
        elif ':' in token:
            equal = token.index(':')
            key = token[:equal]
            value = token[equal + 1:]
            setattr(result, key, value)
        else:
            if result.face is None:
                result.face = token
            else:
                result.face += ' ' + token
    return result

def get_option(config, name, default=None, type=''):
    if not type and default is not None:
        if isinstance(default, bool):
            type = 'boolean'
        elif isinstance(default, int):
            type = 'int'
    try:
        method = getattr(config, 'get%s' % type)
        return method(*name.split('.'))
    except ConfigParser.Error:
        return default

def _zekr_translation_info(path):
    if not zipfile.is_zipfile(path):
        return {}
    zipped = zipfile.ZipFile(path)
    properties = zipped.read('translation.properties').decode('utf-8')
    info = {}
    for line in properties.splitlines():
        if line.strip() == '' or line.lstrip().startswith('#'):
            continue
        equals = line.index('=')
        key = line[:equals].strip()
        value = line[equals + 1:].strip()
        info[key] = value
    return info


def _translation_from_path(path, replacements=[]):
    if not zipfile.is_zipfile(path):
        return quran_from_path(path)
    info = _zekr_translation_info(path)
    zipped = zipfile.ZipFile(path)
    data = zipped.read(info['file'])
    encoding = info['encoding']
    zipped.close()
    return Quran(data.decode(encoding), replacements=replacements)


_DEFAULT_DOT_NOOR = """\
# The default ~/.noor config file
# http://noor.sf.net/

[common]
# the translation file
#translation = /path/to/translation.file
# if you want to override noor's quan text
#quran = /path/to/quran.text

# should noor use arabic aya numbers
#arabic_numbers = 1
# should noor use arabic sura names
#arabic_sura_names = 1

# the language to use; it can be one of: ar, en, fa, ml, ps, tr
#lang = en

# some fonts do not handle some characters or some combinations of
# them correctly; these configs should be a tab-separated list of
# these combinations.  If a token contains '->', the characters after
# it is used as replacement.  For instance ``cd ab->ba`` will hide all
# ``cd``\s and will replace all ``ab``\s with ``ba``\s.
#hide_chars =
#hide_translation_chars =

[gtk]
# the font to use
font = Dejavu Sans Mono size:23 bgcolor:#DDDDFF
translation_font = Dejavu Sans Mono size:14 fgcolor:#444455
# should noor ask which sura to show if no argument is given
ask_sura_at_start = 1

# the program to play sura or aya recitation
#play_sura = mplayer -noconsolecontrols /path/to/folder/${sura}.ogg
#play_aya = mplayer -noconsolecontrols /path/to/folder/${sura}${aya}.mp3
# if true goes to the next aya after the player terminates
#follow_ayas = 1

# controls whether to show the menu bar
#menu_bar = 0
# controls whether to show the status bar
#status_bar = 0
# controls whether to show the scrollbar
#scrollbar = 0
# controls whether to show the task pane
#task_pane = 0
# controls whether to show the toolbar
#toolbar = 0
# toolbar style; can be: text, icons, both
#toolbar_style = both

# whether to hide buttons in search and note panels
#hide_buttons = 1

# controls whether to show notes box
#show_notes = 1
# the location of note files; ~/.noornotes by default
#notes_dir = ~/.noornotes

# uncomment if you want to match case in searches
#ignore_case = 0
# uncomment if you want to match diacritics in searches
#ignore_diacritics = 0

# specify the bookmark file; ~/.noor.bmk by default
#bookmark_file = ~/.noor.bmk

# uncomment to shadow all but the current aya
#shadow_ayas = 1

[html]
# the font to use
font = Dejavu Sans Mono 7
translation_font = Dejavu Sans Mono size:3 fgcolor:444455

[cgi]
# the url noorcgi.py is mapped to in the server
#url = noorcgi.py
"""
