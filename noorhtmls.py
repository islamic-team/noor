#!/usr/bin/env python
import os.path
import sys

from noor import uihelpers, htmlhelper, i18n


def generate_htmls(folder=None):
    if not os.path.exists(folder):
        os.mkdir(folder)
    config = uihelpers.get_config()
    quran = uihelpers.get_quran(config)
    trans = uihelpers.get_translation(config)
    uihelpers.set_lang(config)

    index = open(os.path.join(folder, 'index.html'), 'wb')
    index.write(htmlhelper.get_suralist_page(
                quran, config, _sura_href).encode('utf-8'))
    index.close()
    for i in range(len(quran.suras)):
        text = htmlhelper.get_sura_page(quran, trans, config, i + 1)
        filename = _sura_href(i + 1)
        sura_file = open(os.path.join(folder, filename), 'wb')
        sura_file.write(text.encode('utf-8'))
        sura_file.close()


def _sura_href(number):
    return '%03d.html' % number


if __name__ == '__main__':
    if len(sys.argv) >= 3:
        print 'Usage: %s output_folder' % sys.argv[0]
        sys.exit(1)
    if len(sys.argv) > 1:
        output = sys.argv[1]
    else:
        output = raw_input("Output directory name: ")
    if output:
        print "Generating html files..."
        generate_htmls(output)
        print
        print "Files were generated successfully!"
