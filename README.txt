=============================
 Noor, A python Quran viewer
=============================


Overview
========

Noor_ is a small Quran viewer written in Python_.  It shows the
translation of each aya, if specified, right after it.  These are the
interfaces that come with noor:

===========   ===================================================
Interface     Description
===========   ===================================================
noorgtk       Uses PyGtk_ library
noorhtmls     Outputs quran in html files in the output directory
noorcgi       Can be used as a CGI script in web servers
noortxt       Outputs a sura to a text file
===========   ===================================================

.. _Noor: http://noor.sf.net/
.. _PyGTK: http://www.pygtk.org/
.. _Python: http://www.python.org/


New Features
============

* added arabic_sura_names config
* installing data files according to debian policy

Getting Started
===============

noorgtk
-------

For using the GTK interface you need to install Python_ and PyGTK_
(they are installed by default in some linux distributions like
Ubuntu).  You can start it by running ``noorgtk.pyw`` script either
from the command line or by double clicking it in the extracted
folder.  That is, no installation is necessary.

But if you like, you can run ``python setup.py install`` to install
Noor.  After that you'll be able to run ``noorgtk.pyw`` in the terminal
from any location.

noorhtmls
---------

The noorhtmls interface can be used to output quran in html files in a
directory.  All you should do is to run::

  noorhtmls.py output_folder

The ``index.html`` file will contain the list of suras and
``001.html`` through ``114.html`` files will contain the suras.  Note
that the configuration specified in the ``~/.noor`` file is considered
when generating the pages.

noorcgi
-------

Installing the CGI script, ``noorcgi.py``, is like installing any
other CGI script.  You probably have to copy it to the ``cgi-bin``
directory of your web server.  But before that you should either
install noor as described in the noorgtk_ section or change the line
in ``noorcgi.py`` file which looks like::

  # Add noor to the python path if its not installed:
  sys.path.append('/noor/extracted/folder/')

to point to the folder in which you've extracted noor.  Note that if
you want to use the CGI interface you no longer need to install
PyGTK_.


Noorgtk Tutorial
================

After starting ``noorgtk`` as described above, you'll be asked to
select a sura to view.  (Use ``gtk.ask_sura_at_start`` config or give
a command line argument to skip that).

Now you see the sura you've chosen.  Use up and down keys to move the
text.  Press ``a`` key; you'll be asked to enter an aya number.  After
pressing enter you'll be moved to that aya.

Now press ``c``.  A dialog pops up in which you can select a sura.

Press ``s`` this time; Noor will ask you to insert a sura number to
go to.  Also ``J`` (that is capital j) moves you to the start of a
juz.

Now press ``C-M-n`` or ``C-j`` (whichever you're most comfortable
with).  Noor will show the next sura.  Likewise ``C-M-p`` or ``C-k``
can be used for previous sura.

Some commands need an integer argument, such as goto aya, goto the
page before aya, goto sura and goto juz.  If you type a number before
using these commands, that number will be assumed to be the argument
and Noor *won't* ask the number in a dialog.

Note that many of Noor commands can be done in more than one way and
have more than one key-binding.  See the `noorgtk keys`_ and try its
commands.

Also you can tell noorgtk to start from a sura and aya using command
line arguments.  For instance ``noorgtk.pyw 100 3`` goes to the third
aya of the 100th sura.


Adding Translations
===================

You can edit the ``~/.noor`` file::

  [common]
  translation = /path/to/translation/file

The translation file can be in two formats.  It can be either a zip
file in the same format as Zekr_ translation files which can be
obtained from http://siahe.com/zekr/resources.html or it can be a
utf-8 text file with translation of each aya in one line.

.. _Zekr: http://siahe.com/zekr


Changing Font
=============

You can change the GTK or html fonts in ``~/.noor`` file::

  [gtk]
  font = me_quran 23
  translation_font = Dejavu Sans Mono size:14 fgcolor:#001100 fgcolor:#DDDDFF

  [html]
  font = me_quran 6 fgcolor:554444
  translation_font = Dejavu Sans Mono 4 fgcolor:444455

A font has face, size, fgcolor and bgcolor attributes.  These
attributes can be set using ``attr:value`` format where ``attr`` is
attribute name.  The old format still works; that is ``myfont 16`` is
the same as ``myfont size:16`` and also the same as ``face:myfont
size:16``.


Notes
=====

You can add notes to suras and ayas.  You can use ``gtk.show_notes``
variable to enable this feature; add this to your ``~/.noor``::

  [gtk]
  show_notes = 1

When enabled, notes panel is shown right above the status bar.

Using ``A`` key (that is ``shift-a``), moves the focus to ``notes``
panel (you can also use the mouse).  You can use ``N`` to toggle note
panel visibility.

Notes are saved in ``gtk.notes_dir`` directory (``~/.noornotes``, by
default) when either the focus moves out of notes panel or when you
press ``C-s``.  Notes are saved in:

* notes for kth sura will be written in ``k.txt``
* notes for the kth aya of jth sura will be written in ``j-k.txt``


Changing The Language
=====================

You can change the language of noorgtk dialogs and noorhtmls pages;
for instance::

  [common]
  lang = en

will use English.  The value of ``common.lang`` config should be a two
letter symbol of a language.  Currently Arabic (``ar``), English
(``en``), Farsi (``fa``) and Pashto (``ps``) are supported.  If you
like, you can translate noor to other languages (ask the mailing list
if you need help).


Playing Recitation
==================

You can use ``gtk.play_aya`` and ``gtk.play_sura`` to play sura and
aya recitation.  For instance::

  [gtk]
  play_aya = mplayer -noconsolecontrols /path/to/folder/${sura}${aya}.mp3

``${sura}`` and ``${aya}`` are replaced with zero filled sura and aya
number.

You can also ask noor to follow ayas; that is when the player finishes
with zero return value noor goes to the next aya.  For this to work
you need to set the ``gtk.follow_ayas`` option::

  [gtk]
  follow_ayas = 1

You can use the ``K`` key to stop following ayas.


Hiding Noorgtk Parts
====================

You can hide most GUI parts of noorgtk.  For instance::

  [gtk]
  menu_bar = 0
  status_bar = 0
  toolbar = 0
  scrollbar = 0
  task_pane = 0
  show_notes = 0
  hide_buttons = 1

Will hide things like menu bar, status bar and toolbar.


Noorgtk Keys
============

These keys can be used in the GTK interface.  Note that ``C-`` stands
for control key, ``M-`` for alt key, ``S-`` for shift key and ``C-M``
for control and alt key pressed together.  For instance ``C-n`` means
pressing ``n`` key while holding the control key.

=============================  ============================
Action                         Keys
=============================  ============================
move down                      down, j, C-n, C-f, C-d
move up                        up, k, C-p, C-b, C-d
next page                      page-down, space, C-v
previous page                  page-up, S-space, M-v
sura start                     home, C-a
sura end                       end, C-e

next sura                      C-M-n, C-j
choose sura                    c
previous sura                  C-M-p, C-k

next aya                       n
previous aya                   p
current aya                    .

copy aya                       y
copy aya translation           Y

append digit to number         digits; [0-9]
clear number                   escape, C-g
goto n-th aya                  a, return, M-g, G
goto the page before n-th aya  b, S-return, M-G
goto n-th sura                 s, C-return, C-M-g, s
goto n-th juz start            J, C-M-return, C-M-G

back                           C-[, M-left
forward                        C-], M-right

add bookmark                   B
jump to bookmark               M

add mark                       m
jump to mark                   ', `

fullscreen                     f, F11
search forward                 /, C-s
search backwards               ?, C-r

stop the player                K

toggle notes                   N
write note                     A

about                          r
quit                           q
=============================  ============================

Note ``n`` here means the number typed so far.  Actually by pressing
digits you prefix commands.  For instance pressing ``2`` followed by
``1`` and followed by ``C-M-return``, you tell noor to go to the start
of the 21st juz.

If no prefix is typed for prefixed commands, noor will ask it in a
dialog.


Configuration File
==================

You can specify configuration options in ``~/.noor`` file.  See the
end of ``noor/uihelpers.py`` file for the default contents of
``~/.noor`` file (the default ``~/.noor`` will be created if it does
not exist).

Note that ``html`` section of the config file is applied to both
*noorcgi* and *noorhtmls* interfaces.


Bookmarks
=========

Noor supports aya bookmarks.  When you add a bookmark, the location
and its name is saved so that you can jump to that location from the
``list bookmarks`` dialog.

Noor saves bookmarks in a ``~/.noor.bmk`` by default.  You can change
that by using ``gtk.bookmark_file`` option.  Each line of this file
can contain a bookmark like::

  sura_number:aya_number bookmark_name

You can edit this file manually to change them.


License
=======

This program is under the terms of GNU GPL (GNU General Public
License).  Have a look at ``COPYING`` file for more information.


Source Repository
=================

Noor uses Mercurial_ VCS:

* Main repo: http://bitbucket.org/agr/noor

.. _Mercurial: http://selenic.com/mercurial


Contributing
============

Patches to noor's code are welcome.

Patch style:

* use four spaces for indentation.
* follow :PEP:`8`.
* preferably use ``hg export`` for making patches

You can submit your patches and discuss your ideas in the
`noor-dev [at] googlegroups.com`_ mailing list.

.. _noor-dev [at] googlegroups.com:
   http://groups.google.com/group/noor-dev


Thanks
======

Special thanks to Zekr_ project for their nice program.
