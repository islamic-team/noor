#!/usr/bin/env python
"""Export translation PO files to a new template"""
import sys


def update_po(pot, po):
    result = []
    msgid = ''
    pot_lines = pot.splitlines()
    po_lines = po.splitlines()
    header_end = _first_msg(po_lines)
    result.extend(po_lines[:header_end])
    trans = _parse_po(po)
    for line in pot_lines[_first_msg(pot_lines):]:
        if line.startswith('msgid'):
            msgid = line[6:].strip()
        if line.startswith('msgstr'):
            msgstr = line[7:].strip()
            if trans.get(msgid, None) and msgstr == '""':
                result.append('msgstr %s' % trans.get(msgid, None))
                continue
        result.append(line)
    return '\n'.join(result) + '\n'

def _parse_po(po):
    result = {}
    msgid = ''
    for line in po.splitlines():
        if line.startswith('msgid'):
            msgid = line[6:].strip()
        if line.startswith('msgstr'):
            result[msgid] = line[7:].strip()
    return result

def _first_msg(lines):
    for index, line in enumerate(lines):
        if line.startswith('msgid') and line.strip() != 'msgid ""':
            return index
    return len(lines)

def _read(path):
    file = open(path, 'rb')
    data = file.read()
    file.close()
    return data.decode('utf-8')


if __name__ == '__main__':
    if len(sys.argv) != 4:
        print 'updates po files:'
        print '  %s <old_po> <new_pot> <output_po>' % sys.argv[0]
        sys.exit(0)
    po = _read(sys.argv[1])
    pot = _read(sys.argv[2])
    updated = update_po(pot, po)
    output = open(sys.argv[3], 'wb')
    output.write(updated.encode('utf-8'))
    output.close()
