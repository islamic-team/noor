#!/usr/bin/env python
import os
import subprocess
import sys
import glob

import noor


def make_mo_files():
    for lang in os.listdir(_localedir()):
        podir = os.path.join(_localedir(), lang, 'LC_MESSAGES')
        po = os.path.join(podir, 'noor.po')
        mo = os.path.join(podir, 'noor.mo')
        if os.path.exists(po):
            try:
                _execute(['msgfmt', '-o', mo, po])
                print 'Generated %s' % mo
            except OSError:
                pass

def make_pot_file():
    noordir = _noordir()
    base = os.path.join(noordir, os.pardir)
    pot = os.path.join(_localedir(), 'noor.pot')
    if os.path.samefile(os.curdir, base):
        base = ''
        noordir = 'noor'
    files = []
    files.extend(glob.glob(os.path.join(base, '*.py')))
    files.extend(glob.glob(os.path.join(base, '*', '*.py')))
    files.extend(glob.glob(os.path.join(base, '*', '*', '*.py')))
    print '\n'.join(files)
    args = ['pygettext', '-o', pot]
    args.extend(files)
    try:
        _execute(args)
        print 'Generated %s' % pot
    except OSError:
        pass

def _execute(args):
    process = subprocess.Popen(args, stdout=subprocess.PIPE)
    return process.stdout.read()

def _noordir():
    return os.path.dirname(noor.__file__)

def _localedir():
    return os.path.join(_noordir(), 'locale')


if __name__ == '__main__':
    if 'pot' in sys.argv:
        make_pot_file()
    else:
        make_mo_files()
