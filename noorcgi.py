#!/usr/bin/env python
import cgi
import cgitb
import os
import sys


cgitb.enable()
# Add noor to the python path if its not installed:
sys.path.append('/home/ali/projects/noor')
from noor import uihelpers, htmlhelper, i18n


def handle_request(query_string=None):
    query = {}
    if query_string is not None:
        query = cgi.parse_qs(query_string)

    config = uihelpers.get_config()
    quran = uihelpers.get_quran(config)
    uihelpers.set_lang(config)
    trans = uihelpers.get_translation(config)

    if 'sura' in query:
        sura_number = int(query['sura'][0])
        return htmlhelper.get_sura_page(
            quran, trans, config, sura_number).encode('utf-8')
    else:
        url = uihelpers.get_option(config, 'cgi.url', 'noorcgi.py')
        def _sura_href(number):
            return '%s?sura=%s' % (url, number)
        return htmlhelper.get_suralist_page(
            quran, config, _sura_href).encode('utf-8')


print 'Content-Type: text/html'
print
query_string = os.environ.get('QUERY_STRING', None)
print handle_request(query_string)
