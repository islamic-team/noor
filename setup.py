import os
from distutils.core import setup
from glob import glob

import noor

data_files=[('share/applications/', ['contrib/noor.desktop']),
            ('share/pixmaps/', ['contrib/noor.png']),
            ('share/pixmaps/noor/', glob('noor/data/icons/*.png')),
            ('share/noor/data/', glob('noor/data/quran-*'))]
locales = []
for folder in glob('noor/locale/*/LC_MESSAGES'):
    locales.append(('share/' + folder.split(os.path.sep, 1)[1],
                   [folder + '/noor.mo']))
data_files.extend(locales)

classifiers=[
    'Development Status :: 4 - Beta',
    'Operating System :: OS Independent',
    'Environment :: X11 Applications :: GTK',
    'Environment :: Console',
    # Have not been tested on MacOS
    # 'Environment :: MacOS X',
    'Intended Audience :: End Users/Desktop',
    'License :: OSI Approved :: GNU General Public License (GPL)',
    'Natural Language :: English',
    'Programming Language :: Python',
    'Topic :: Religion']

def get_long_description():
    lines = open('README.txt').read().splitlines(False)
    end = lines.index('License')
    return '\n' + '\n'.join(lines[:end]) + '\n'

def make_mo_files():
    path = os.path.join('contrib', 'generate_translations.py')
    module_globals = {}
    execfile(path, module_globals)
    module_globals['make_mo_files']()

make_mo_files()

setup(name='noor',
      version=noor.VERSION,
      description='A python Quran viewer',
      long_description=get_long_description(),
      author='Ali Gholami Rudi',
      author_email='aligrudi@users.sourceforge.net',
      url='http://noor.sf.net/',
      packages=['noor', 'noor.gtkhelper'],
      scripts=['noorgtk.pyw', 'noorhtmls.py', 'noortxt.py'],
      data_files=data_files,
      license='GNU GPL',
      classifiers=classifiers)
