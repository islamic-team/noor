#!/usr/bin/env python
import random
import sys

import gtk
import pango

import noor.gtkhelper.ask
import noor.gtkhelper.bookmark
import noor.gtkhelper.history
import noor.gtkhelper.keys
import noor.gtkhelper.mark
import noor.gtkhelper.menu
import noor.gtkhelper.minibuffer
import noor.gtkhelper.note
import noor.gtkhelper.player
import noor.gtkhelper.search
import noor.gtkhelper.shadow
import noor.gtkhelper.status
import noor.gtkhelper.suras
import noor.gtkhelper.task
import noor.gtkhelper.tool
import noor.gtkhelper.writer
import noor.i18n
from noor import uihelpers, quran


def repeatable(func):
    def newfunc(self, *args, **kwds):
        count = 1
        if self.number is not None:
            count = self.number
            self._clear_number()
        for i in range(count):
            func(self, *args, **kwds)
    return newfunc


class NoorWindow(object):

    def __init__(self, config, quran, sura=None):
        self.config = config
        self.quran = quran
        self.trans = uihelpers.get_translation(config)
        self.current_sura = None
        self.current_aya = 0
        self.number = None

        self.hooks = {'sura': [], 'aya': [], 'sura_planned': [], 'kill': []}

        self.history = noor.gtkhelper.history.History(self)
        self.marks = noor.gtkhelper.mark.Marks(self)
        self.player = noor.gtkhelper.player.Player(self)
        noor.gtkhelper.shadow.init_shadow(self)
        self._init_keys()
        self._init_window()

        self.show_sura()

    def _init_window(self):
        self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        self.is_fullscreen = False
        self.window.set_border_width(1)
        self.window.set_size_request(800, 600)
        self.window.connect('destroy', self._quit)
        vbox = gtk.VBox(False, 1)
        self.window.add(vbox)
        self.text_view = gtk.TextView()

        self.menu_bar = noor.gtkhelper.menu.make_menu_bar(self)
        if self.menu_bar is not None:
            vbox.pack_start(self.menu_bar, False, False, 0)

        self.toolbar = noor.gtkhelper.tool.make_toolbar(self)
        if self.toolbar is not None:
            vbox.pack_start(self.toolbar, False, False, 0)

        self.sura_window = gtk.ScrolledWindow()
        self.sura_window.add(self.text_view)
        scroll_policy = gtk.POLICY_ALWAYS
        if not uihelpers.get_option(self.config, 'gtk.scrollbar', True):
            scroll_policy = gtk.POLICY_NEVER
        self.sura_window.set_policy(gtk.POLICY_AUTOMATIC, scroll_policy)
        self.text_view.connect('key_press_event', self._key_pressed)

        self.task_pane = noor.gtkhelper.task.create_task_pane(self)
        if self.task_pane is not None:
            hbox = gtk.HBox(False, 1)
            vbox.pack_start(hbox, True, True, 0)
            hbox.pack_start(self.sura_window, True, True, 0)
            hbox.pack_start(self.task_pane, False, False, 0)
        else:
            vbox.pack_start(self.sura_window, True, True, 0)

        self.adjustment = self.sura_window.get_vscrollbar().get_adjustment()
        self.text_view.grab_focus()
        self.minibuffer = noor.gtkhelper.minibuffer.MiniBuffer(self)
        vbox.pack_start(self.minibuffer.minibuffer, False, False, 0)

        self.notes = noor.gtkhelper.note.Notes(self)
        if self.notes.box:
            vbox.pack_start(self.notes.box, False, False, 0)

        self.status = noor.gtkhelper.status.Status(self)
        if self.status.status_bar is not None:
            vbox.pack_start(self.status.status_bar, False, False, 0)

    def _init_keys(self):
        self._key_pressed = noor.gtkhelper.keys.Keys(
            self.keys, self.s_keys, self.c_keys, self.m_keys,
            self.cm_keys, args=[self])

    def add_hook(self, name, callback):
        self.hooks[name].append(callback)

    def remove_hook(self, name, callback):
        self.hooks[name].remove(callback)

    def _call_hooks(self, name, *args, **kwds):
        for hook in self.hooks[name]:
            hook(*args, **kwds)

    def show_sura(self, sura=None):
        if sura is not None and 0 < sura <= 114:
            self._call_hooks('sura_planned')
            self._clear_all()
            self.current_sura = sura
            self.current_aya = 0
            self._set_title()
            collector = noor.gtkhelper.writer.GtkAyaWriter(self.text_view, self.config)
            uihelpers.write_sura(self.quran, self.config, sura,
                                 collector, self.trans)

            self._create_location_tag()
            self._call_hooks('sura')
        if self.current_sura is None:
            self._show_home()

    def _create_location_tag(self):
        text_buffer = self.text_view.get_buffer()
        text_buffer.create_mark('location', text_buffer.get_start_iter())

    @property
    def dialog_font(self):
        return _get_font(self.config, 'gtk.translation_font', size_diff=-3)

    def _clear_all(self):
        self._set_title()
        self.text_view.get_buffer().set_text('')
        self.text_view.set_buffer(gtk.TextBuffer())

    def _show_home(self, *args, **kwds):
        self._call_hooks('sura_planned')
        self.current_sura = None
        self._clear_all()
        _insert_home(self.config, self.text_view)
        self.current_aya = 0
        self._create_location_tag()
        self._call_hooks('sura')

    def _show_about(self, *args, **kwds):
        dialog = gtk.Dialog()
        dialog.set_title('About Noor')
        dialog.set_border_width(1)
        def quit(*args, **kwds):
            dialog.hide()
            gtk.main_quit()
        dialog.connect('destroy', lambda *args: quit())
        dialog.connect('delete_event', lambda *args: quit())
        box = dialog.vbox
        label = gtk.Label()
        label.set_text(
            'Noor, A Python Quran Viewer\n\n'
            'Version: %s\nHomepage: http://noor.sf.net\n\n%s\n'
            % (noor.VERSION, noor.COPYRIGHT))
        box.pack_start(label, True, True, 0)
        button = gtk.Button()
        button.connect('clicked', quit)
        button_label = gtk.Label()
        button_label.set_markup(_('OK'))
        button.add(button_label)
        box.pack_start(button, False, False, 0)
        dialog.show_all()
        gtk.main()

    def _set_title(self):
        title = '%s - %s' % (_('Noor'), self._displayed_page_name())
        self.window.set_title(title)

    def _displayed_page_name(self):
        if self.current_sura is not None:
            if uihelpers.get_option(self.config,
                                    'common.arabic_sura_names', True):
                return self.sura.name
            else:
                return self.sura.english_name
        else:
            return _('About')

    def _back(self, *args, **kwds):
        self.history.back()

    def _forward(self, *args, **kwds):
        self.history.forward()

    @property
    def sura(self):
        if self.current_sura:
            return self.quran.suras[self.current_sura - 1]

    def _goto_sura(self, *args, **kwds):
        number = self._get_number(_('Which Sura?'))
        self.show_sura(number)

    def _goto_juz(self, *args, **kwds):
        juz = self._get_number(_('Which Juz?'))
        if 0 < juz <= 30:
            sura, aya = self.quran.juz_starts[juz - 1]
            if self.current_sura != sura:
                self.show_sura(sura)
            self.current_aya = aya
            self._goto_mark(aya, location=0.1)

    def _ask_sura(self, *args, **kwds):
        sura = noor.gtkhelper.suras.ask_sura(self.config, self.quran)
        self.show_sura(sura)

    def _adjust_page(self, increment):
        new_value = self.adjustment.value + increment
        newvalue = max(self.adjustment.lower,
                       min(new_value, self.adjustment.upper
                           - self.adjustment.page_size))
        self.adjustment.set_value(newvalue)

    @repeatable
    def _prev_line(self):
        self._adjust_page(-self.adjustment.step_increment)

    @repeatable
    def _next_line(self):
        self._adjust_page(self.adjustment.step_increment)

    @repeatable
    def _prev_page(self):
        self._adjust_page(-self.adjustment.page_increment)

    @repeatable
    def _next_page(self):
        self._adjust_page(self.adjustment.page_increment)

    def _prev_sura(self, *args, **kwds):
        if self.current_sura is not None:
            self.show_sura(self.current_sura - 1)

    def _next_sura(self, *args, **kwds):
        if self.current_sura is not None:
            self.show_sura(self.current_sura + 1)

    def _start(self):
        self._adjust_page(self.adjustment.lower - self.adjustment.value)

    def _end(self):
        self._adjust_page(self.adjustment.upper - self.adjustment.value
                          - self.adjustment.page_size)

    def _quit(self, *args):
        self._call_hooks('kill')
        gtk.main_quit()

    def _redraw(self):
        self.window.queue_draw()

    def _clear_number(self):
        self.number = None
        self.status.prefix()

    def _escaped(self, *args):
        self._clear_number()
        if self.is_fullscreen:
            self._fullscreen()

    def _backspace_number(self):
        if self.number is not None:
            self.number //= 10
            if self.number == 0:
                self.number = None
            self.status.prefix()

    def _goto_mark(self, number, location=0.0):
        textbuffer = self.text_view.get_buffer()
        mark = textbuffer.get_mark(str(number))
        if mark is not None:
            self.text_view.scroll_to_mark(mark, 0, True, 0.0, location)
            textbuffer.move_mark_by_name('location',
                                         textbuffer.get_iter_at_mark(mark))

    def show_aya(self, number):
        if self.current_sura is None:
            return
        if 0 <= number <= self.sura_ayas:
            self._goto_mark(number)
            self.current_aya = number
            self._call_hooks('aya')

    def _goto_aya(self, *args, **kwds):
        number = self._get_number(_('Which Aya?'))
        self.show_aya(number)

    def _goto_random_aya(self, *args, **kwds):
        if self.current_sura is None:
            return
        aya = random.randint(1, self.sura_ayas)
        self.show_aya(aya)

    @property
    def sura_ayas(self):
        return len(self.quran.suras[self.current_sura - 1].ayas)

    def _get_number(self, message):
        result = self.number
        self.number = None
        self.status.prefix()
        if result is None:
            try:
                return int(noor.gtkhelper.ask.ask(message, message))
            except ValueError:
                pass
        return result

    @repeatable
    def _next_aya(self, *args, **kwds):
        self.show_aya(self.current_aya + 1)

    @repeatable
    def _prev_aya(self, *args, **kwds):
        self.show_aya(self.current_aya - 1)

    def _current_aya(self, *args, **kwds):
        self.show_aya(self.current_aya)

    def _goto_before_aya(self, *args, **kwds):
        number = self._get_number(_('Which Aya?'))
        self._goto_mark(number, location=1.0)

    def run(self):
        self.window.show_all()
        self.minibuffer.hide()
        gtk.main()

    def _fullscreen(self, *args, **kwds):
        if not self.is_fullscreen:
            self.window.fullscreen()
        else:
            self.window.unfullscreen()
        self.is_fullscreen = not self.is_fullscreen
        for widget in [self.toolbar, self.menu_bar,
                       self.status.status_bar, self.task_pane]:
            if not widget:
                continue
            if self.is_fullscreen:
                widget.hide()
            else:
                widget.show()

    def _search(self, backward=False):
        searcher = noor.gtkhelper.search.Searcher(self)
        searcher.search()
        if backward:
            searcher.backward()

    def _search_forward(self, *args, **kwds):
        self._search()

    def _search_backward(self, *args, **kwds):
        self._search(True)

    def _add_bookmark(self, *args, **kwds):
        noor.gtkhelper.bookmark.add_bookmark(self)

    def _list_bookmarks(self, *args, **kwds):
        noor.gtkhelper.bookmark.list_bookmarks(self)

    def _stop_player(self, *args, **kwds):
        self.player.stop()

    def _add_note(self, *args, **kwds):
        self.notes.write_note()

    def _toggle_note(self, *args, **kwds):
        self.notes.toggle()

    def _copy_to_clipboard(self, text):
        board = gtk.clipboard_get()
        primary = gtk.clipboard_get("PRIMARY")
        board.set_text(text)
        if primary:
            primary.set_text(text)

    def _copy_aya(self, *args, **kwds):
        if self.current_sura and self.current_aya:
            text = quran.get_aya(self.quran, self.current_sura,
                                 self.current_aya)
            self._copy_to_clipboard(text)

    def _copy_trans(self, *args, **kwds):
        if self.current_sura and self.current_aya:
            text = quran.get_aya(self.trans, self.current_sura,
                                 self.current_aya)
            self._copy_to_clipboard(text)

    def _mark(self, *args, **kwds):
        mark = noor.gtkhelper.ask.ask_key()
        if mark is not None:
            self.marks.add_mark(mark)

    def _jump(self, *args, **kwds):
        mark = noor.gtkhelper.ask.ask_key()
        if mark is not None:
            self.marks.jump_mark(mark)

    keys = {
        gtk.keysyms.BackSpace: _backspace_number,
        gtk.keysyms.Down: _next_line,
        gtk.keysyms.End: _end,
        gtk.keysyms.Escape: _escaped,
        gtk.keysyms.F11: _fullscreen,
        gtk.keysyms.Home: _start,
        gtk.keysyms.Next: _next_page,
        gtk.keysyms.Prior: _prev_page,
        gtk.keysyms.Return: _goto_aya,
        gtk.keysyms.Up: _prev_line,
        ord(' '): _next_page,
        ord('.'): _current_aya,
        ord('/'): _search_forward,
        ord('a'): _goto_aya,
        ord('b'): _goto_before_aya,
        ord('c'): _ask_sura,
        ord('f'): _fullscreen,
        ord('j'): _next_line,
        ord('k'): _prev_line,
        ord('m'): _mark,
        ord('`'): _jump,
        ord('\''): _jump,
        ord('n'): _next_aya, ord('p'): _prev_aya,
        ord('q'): _quit, ord('r'): _show_home,
        ord('s'): _goto_sura,
        ord('y'): _copy_aya,}
    c_keys = {
        gtk.keysyms.Return: _goto_sura,
        ord('['): _back,
        ord(']'): _forward,
        ord('a'): _start,
        ord('b'): _prev_page,
        ord('d'): _next_page,
        ord('e'): _end,
        ord('f'): _next_page,
        ord('g'): _clear_number,
        ord('j'): _next_sura,
        ord('k'): _prev_sura,
        ord('l'): _redraw,
        ord('n'): _next_line,
        ord('p'): _prev_line,
        ord('r'): _search_backward,
        ord('s'): _search_forward,
        ord('u'): _prev_page,
        ord('v'): _next_page,}
    m_keys = {
        gtk.keysyms.Left: _back,
        gtk.keysyms.Right: _forward,
        ord('G'): _goto_before_aya,
        ord('g'): _goto_aya,
        ord('v'): _prev_page,}
    s_keys = {
        gtk.keysyms.Return: _goto_before_aya,
        ord(' '): _prev_page,
        ord('?'): _search_backward,
        ord('G'): _goto_aya,
        ord('J'): _goto_juz,
        ord('K'): _stop_player,
        ord('B'): _add_bookmark,
        ord('M'): _list_bookmarks,
        ord('A'): _add_note,
        ord('N'): _toggle_note,
        ord('Y'): _copy_trans,}
    cm_keys = {
        gtk.keysyms.Return: _goto_juz,
        ord('G'): _goto_juz,
        ord('g'): _goto_sura,
        ord('n'): _next_sura,
        ord('p'): _prev_sura,}

    for d in range(10):
        def callback(self, d=d):
            self._digit(d)
        keys[ord(str(d))] = callback

    def _digit(self, d):
        if self.number is None:
            self.number = 0
        self.number = self.number * 10 + d
        self.status.prefix()


def _get_font(config, name='gtk.font', size_diff=0):
    raw_font = uihelpers.get_option(config, name, None)
    if raw_font is not None:
        font = uihelpers.parse_font(raw_font)
        face = font.face
        size = font.size
        if face is None:
            face = ''
        if size and size.isdigit():
            size = str(int(font.size) + size_diff)
        else:
            size = ''
        return pango.FontDescription(face + ' ' + size)

def _modify_font(widget, font):
    if font is not None:
        widget.modify_font(font)
    if isinstance(widget, gtk.Container):
        for child in widget.get_children():
            _modify_font(child, font)


def _insert_home(config, view):
    view.set_editable(False)
    view.set_cursor_visible(False)
    view.set_wrap_mode(gtk.WRAP_WORD)
    buffer = view.get_buffer()
    font = uihelpers.parse_font(uihelpers.get_option(
            config, 'gtk.translation_font', ''))
    if font is not None and font.face is not None:
        face = font.face + ' '
    else:
        face = ''
    tag1 = buffer.create_tag('noor', font=face + '55', foreground='#AAFFAA',
                             justification='center')
    tag2 = buffer.create_tag('version', font=face + '20', foreground='#BBBBAA',
                             justification='center')
    tag3 = buffer.create_tag('info', font='mono 13')
    tag4 = buffer.create_tag('copyright', font='15')
    version = uihelpers._int_to_str(noor.VERSION, config=config)
    buffer.insert_with_tags(buffer.get_end_iter(),
                            '\n%s    ' % _('Noor'), tag1)
    buffer.insert_with_tags(buffer.get_end_iter(),
                            ' %s\n\n\n' % version, tag2)
    info = 'Homepage:     http://noor.sf.net\n' + \
           'Mailing list: http://groups.google.com/group/noor-dev' + \
           ' <noor-dev@googlegroups.com>\n' + \
           'Source repo:  http://bitbucket.org/agr/noor\n\n'
    buffer.insert_with_tags(buffer.get_end_iter(), info, tag3)
    buffer.insert_with_tags(buffer.get_end_iter(), noor.COPYRIGHT, tag4)


def show(sura=None, aya=None):
    config = uihelpers.get_config()
    quran = uihelpers.get_quran(config)
    uihelpers.set_lang(config)
    window = NoorWindow(config, quran, sura)
    if sura is None and uihelpers.get_option(
       config, 'gtk.ask_sura_at_start', True):
        sura = noor.gtkhelper.suras.ask_sura(config, quran)
    if sura is not None:
        window.show_sura(sura)
    if aya is not None:
        window.show_aya(aya)
    window.run()


def print_usage():
    print 'Usage: %s [sura_number] [aya_number]' % sys.argv[0]
    sys.exit(1)

if __name__ == '__main__':
    sura = None
    aya = None
    if len(sys.argv) > 1:
        if not sys.argv[1].isdigit():
            print_usage()
        sura = int(sys.argv[1])
    if len(sys.argv) > 2:
        if not sys.argv[2].isdigit():
            print_usage()
        aya = int(sys.argv[2])
    show(sura, aya)
