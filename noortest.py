import unittest

import noor.quran
from noor import uihelpers


class NoorTest(unittest.TestCase):

    def setUp(self):
        super(NoorTest, self).setUp()

    def tearDown(self):
        super(NoorTest, self).tearDown()

    def test_sura_count(self):
        quran = noor.quran.quran_from_path()
        self.assertEquals(114, len(quran.suras))

    def test_sura_ayas(self):
        quran = noor.quran.quran_from_path()
        self.assertEquals(7, len(quran.suras[0].ayas))

    def test_sura_juz(self):
        quran = noor.quran.quran_from_path()
        self.assertEquals([1], quran.suras[0].juz)

    def test_sura_juz_multiple_juzes(self):
        quran = noor.quran.quran_from_path()
        self.assertEquals([12, 13], quran.suras[11].juz)

    def test_sura_juz_last(self):
        quran = noor.quran.quran_from_path()
        self.assertEquals([30], quran.suras[113].juz)

    def test_sura_between_juzes(self):
        quran = noor.quran.quran_from_path()
        self.assertEquals([27], quran.suras[55].juz)

    def test_sura_starting_juz(self):
        quran = noor.quran.quran_from_path()
        self.assertEquals([29], quran.suras[66].juz)


class UIHelpersTest(unittest.TestCase):

    def test_parse_font1(self):
        font = uihelpers.parse_font('myfont')
        self.assertEquals('myfont', font.face)
        self.assertTrue(font.size is None)

    def test_parse_font2(self):
        font = uihelpers.parse_font('myfont 2')
        self.assertEquals('myfont', font.face)
        self.assertEquals('2', font.size)

    def test_parse_font3(self):
        font = uihelpers.parse_font('myfont size:2')
        self.assertEquals('myfont', font.face)
        self.assertEquals('2', font.size)

    def test_parse_font4(self):
        font = uihelpers.parse_font('my font fgcolor:blue bgcolor:white')
        self.assertEquals('my font', font.face)
        self.assertTrue(font.size is None)
        self.assertEquals('blue', font.fgcolor)


if __name__ == '__main__':
    unittest.main()
