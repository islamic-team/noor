#!/usr/bin/env python
import sys
import textwrap

from noor import uihelpers


def write_aya(text, number=None, sajda=None, translation=None, juz_start=None):
    result = []
    if juz_start is not None:
        result.append('<%s>\n\n' % juz_start)
    result.append(textwrap.fill(text))

    if number is not None:
        sign = number
        if sajda == 'minor':
            sign = '*%s*' % sign
        if sajda == 'major':
            sign = '**%s**' % sign
        result.append('[%s]\n' % sign)
    else:
        result.append('\n')
    if translation is not None:
        result.append('%s\n' % textwrap.fill(translation))
    result.append('\n')
    return ''.join(result)


def get_sura(number):
    config = uihelpers.get_config()
    quran = uihelpers.get_quran(config)
    trans = uihelpers.get_translation(config)
    return uihelpers.write_sura(quran, config, number, write_aya, trans)


if __name__ == '__main__':
    if len(sys.argv) == 1 or not sys.argv[1].isdigit():
        print 'Usage: %s sura_number [output_file]' % sys.argv[0]
    else:
        number = int(sys.argv[1])
        output = sys.stdout
        if len(sys.argv) > 2:
            output = open(sys.argv[2], 'wb')
        output.write(get_sura(number).encode('utf-8'))
